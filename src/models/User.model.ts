export class User {
  // Optioneel description attribuut
  constructor(public _id: string,
              public userName: string,
              public email: string,
              public password: string,
              public role: string) {
  }
}

