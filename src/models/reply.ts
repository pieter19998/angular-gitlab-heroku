export class Reply {
  constructor(public _id: string,
              public user: string,
              public content: string,
              public postDatetime: number,
              public replies: Reply[],
              public image?: string) {
  }
}
