import {Reply} from './reply';

export class Thread {
  constructor(
    public _id: string,
    public title: string,
    public content: string,
    public user: string,
    public imageUrl: string,
    public postDatetime: number,
    public reply: Reply[],
    public image?: string) {
  }
}
