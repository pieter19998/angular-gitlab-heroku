import {Thread} from './thread.model';

export class Board {
  // Optioneel description attribuut
  constructor(
    public _id: string,
    public name: string,
    public description: string,
    public threads: Thread[]) {
  }
}
