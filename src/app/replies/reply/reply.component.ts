import {Component, Input, OnInit} from '@angular/core';
import {Reply} from '../../../models/reply';
import {Observable, of, Subscription} from 'rxjs';
import {AuthenticationService} from '../../services/authentication.service';
@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.scss']
})
export class ReplyComponent implements OnInit {
  @Input() reply: Reply
  isLoggedIn$: Observable<boolean>;
  userName = '';
  idSubscription: Subscription;
  userId: string;

  public isCollapsed = true;
  constructor(private authService: AuthenticationService) {
    this.isLoggedIn$ = this.authService.userIsLoggedIn;
    this.idSubscription = this.authService.userId.subscribe(name => this.userId = name);

  }

  ngOnInit() {
  }

}
