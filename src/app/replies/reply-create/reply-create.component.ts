import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Reply} from '../../../models/reply';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Subscription} from 'rxjs';
import {ReplyService} from '../../services/reply.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-reply-create',
  templateUrl: './reply-create.component.html',
  styleUrls: ['./reply-create.component.scss']
})
export class ReplyCreateComponent implements OnInit, OnDestroy {
  @Input() threadId: number;
  replyForm: FormGroup;
  idSubscription: Subscription;
  userId: string;
  imageSrc: string;
  @Input() type: string;

  constructor(private replyService: ReplyService, private authenticationService: AuthenticationService, private formBuilder: FormBuilder) { }

  onSubmit() {
    if (this.replyForm.valid) {
      const content = this.replyForm.value.content;
      const reply = new Reply(null, this.userId, content, Date.now(), null, this.imageSrc);
      this.replyService.addReply(reply, this.threadId, this.type)
    } else {
      console.error('loginForm invalid')
    }
  }

  ngOnInit() {
    this.idSubscription = this.authenticationService.userId.subscribe(name => this.userId = name);
    this.replyForm = new FormGroup({
      content: new FormControl(null, [Validators.required, this.validContent.bind(this)])
    })

    console.log(this.type)
    console.log(this.threadId)
  }

  ngOnDestroy() {
    this.idSubscription.unsubscribe();
  }

  onSelectFile(event) {
    console.log('onSelectFile');
    if (event.target.files && event.target.files[0]) {
      const imageFile: File = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      console.dir(event.target.files[0]);
      reader.onload = (event) => {
        // called once readAsDataURL is completed
        // set the image value to the Base64 string -> can be saved in dtb
        this.replyForm.patchValue({
          imagename: imageFile.name,
          data: reader.result
        });
        // set the image src -> so that it can be displayed as preview
        this.imageSrc = reader.result as string;
        console.log(reader.result as string)
      }
    }
  }

  validContent(control: FormControl): { [s: string]: boolean } {
    return null
    // const content = control.value;
    // if (content.length >= 1) {
    //   return  null
    // } else {
    //   return {content: false}
    // }
  }
}
