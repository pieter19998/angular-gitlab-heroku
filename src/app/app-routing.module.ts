import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'
import { HomeScreenComponent } from './core/home-screen/home-screen.component'
import { UserEditComponent } from './user-edit/user-edit.component'
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component'
import {BoardCreateComponent} from './board-create/board-create.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'userEdit', component: UserEditComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'home', component: HomeScreenComponent},
  { path: 'about', component: UsecasesComponent },
  { path: 'boardCreate', component: BoardCreateComponent },
  { path: '**', component: PageNotFoundComponent}
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
