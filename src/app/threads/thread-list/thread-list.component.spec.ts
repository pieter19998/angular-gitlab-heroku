import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Thread} from '../../../models/thread.model'
import {ThreadService} from '../../services/thread.service';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import { ThreadListComponent } from './thread-list.component';
import {ThreadRoutesModule} from '../thread-routing';

describe('ThreadListComponent', () => {
  let component: ThreadListComponent;
  let fixture: ComponentFixture<ThreadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ThreadRoutesModule],
      declarations: [ ThreadListComponent ],
      providers: [{
        provide: ThreadService
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
