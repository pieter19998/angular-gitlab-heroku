import {Component, OnInit} from '@angular/core';
import {Thread} from '../../../models/thread.model'
import {ThreadService} from '../../services/thread.service';
import {Observable, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-thread-list',
  templateUrl: './thread-list.component.html',
  styleUrls: ['./thread-list.component.scss']
})
export class ThreadListComponent implements OnInit {

  thread: Thread[];
  // rep: Reply[];
  isLoggedIn$: Observable<boolean>;
  userNameSubscription: Subscription;
  userName = '';
  public isCollapsed = true;

  constructor(private threatService: ThreadService, private route: ActivatedRoute, private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.getThread()
    this.isLoggedIn$ = this.authService.userIsLoggedIn;
    this.userNameSubscription = this.authService.userFullName.subscribe(name => this.userName = name);
  }

  getThread(): void {
    this.route.paramMap.subscribe(params => {
      this.threatService.getThread(params.get('id')).subscribe(boards => (this.thread = boards))
    })

  }
}
