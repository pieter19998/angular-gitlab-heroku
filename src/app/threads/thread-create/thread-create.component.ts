import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms'
import {ThreadService} from '../../services/thread.service'
import {AuthenticationService} from '../../services/authentication.service';
import {Thread} from '../../../models/thread.model';
import {Subscription} from 'rxjs';
import {Board} from '../../../models/board.model';

@Component({
  selector: 'app-thread-create',
  templateUrl: './thread-create.component.html',
  styleUrls: ['./thread-create.component.scss']
})

export class ThreadCreateComponent implements OnInit, OnDestroy {
  @Input() board: Board;
  threadForm: FormGroup;
  idSubscription: Subscription;
  userId: string;
  imageSrc: string;
  public isCollapsed = true;

  constructor(private threadService: ThreadService, private authenticationService: AuthenticationService, private formBuilder: FormBuilder) {
  }

  onSubmit() {
    if (this.threadForm.valid) {
      const title = this.threadForm.value.title;
      const content = this.threadForm.value.content;
      const image = this.threadForm.value.file;
      const thread = new Thread(null, title, content, this.userId, this.imageSrc, Date.now(), null, this.imageSrc);
      this.threadService.addThead(thread, this.board)
    } else {
      console.error('loginForm invalid')
    }
  }

  ngOnInit() {
    this.idSubscription = this.authenticationService.userId.subscribe(name => this.userId = name);
    this.threadForm = new FormGroup({
      title: new FormControl(null, [Validators.required, this.validTitle.bind(this)]),
      content: new FormControl(null, [Validators.required, this.validContent.bind(this)])
    })
  }

  ngOnDestroy() {
    this.idSubscription.unsubscribe();
  }

  onSelectFile(event) {
    console.log('onSelectFile');
    if (event.target.files && event.target.files[0]) {
      const imageFile: File = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      console.dir(event.target.files[0]);
      reader.onload = (event) => {
        // called once readAsDataURL is completed
        // set the image value to the Base64 string -> can be saved in dtb
        this.threadForm.patchValue({
          imagename: imageFile.name,
          data: reader.result
        });
        // set the image src -> so that it can be displayed as preview
        this.imageSrc = reader.result as string;
        console.log(reader.result as string)
      }
    }
  }

  validTitle(control: FormControl): { [s: string]: boolean } {
    // const title = control.value;
    // if (title.length >= 1) {
    //   return  null
    // } else {
    //   return {title: false}
    // }
    return null
  }

  validContent(control: FormControl): { [s: string]: boolean } {
    return null
    // const content = control.value;
    // if (content.length >= 1) {
    //   return  null
    // } else {
    //   return {content: false}
    // }
  }
}

