import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ThreadListComponent } from './thread-list/thread-list.component'
import { ThreadCreateComponent } from './thread-create/thread-create.component'
import { ReplyComponent } from '../replies/reply/reply.component'

const ThreadRoutes: Routes = [
  { path: 'thread/:id', component: ThreadListComponent,
    children: [
      {path: 'reply' , component: ReplyComponent},
    ]
  },
]

@NgModule({
  imports: [RouterModule.forChild(ThreadRoutes)],
  exports: [RouterModule]
})
export class ThreadRoutesModule {}
