import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { DashboardComponent } from './core/dashboard/dashboard.component'
import { EntitiesComponent } from './entities/entities.component'
import { BoardComponent } from './board/board.component'

// mockdata
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api'
import { InMemoryDataService } from './services/in-memory-data.service'
import { HttpClientModule } from '@angular/common/http'
import {AlertModule} from './alert/alert.module';
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ThreadComponent} from './board/thread/thread.component';
import { ThreadCreateComponent } from './threads/thread-create/thread-create.component';
import { ThreadListComponent } from './threads/thread-list/thread-list.component';
import { BoardRoutesModule } from './board/board-routing.module';
import { ThreadRoutesModule } from './threads/thread-routing';
import { HomeScreenComponent } from './core/home-screen/home-screen.component';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import { HeaderComponent } from './core/header/header.component';
import { ReplyComponent } from './replies/reply/reply.component';
import { ReplyCreateComponent } from './replies/reply-create/reply-create.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { BoardCreateComponent } from './board-create/board-create.component';
import { AlertComponent } from './alert/alert.component';
import {AlertService} from './alert/alert.service';


@NgModule({
  declarations: [
    AppComponent,
    UsecasesComponent,
    UsecaseComponent,
    DashboardComponent,
    EntitiesComponent,
    LoginComponent,
    RegisterComponent,
    BoardComponent,
    ThreadComponent,
    ThreadCreateComponent,
    ThreadListComponent,
    HomeScreenComponent,
    PageNotFoundComponent,
    HeaderComponent,
    ReplyComponent,
    ReplyCreateComponent,
    UserEditComponent,
    BoardCreateComponent,
  ],
  imports: [
    HttpClientModule,
    AlertModule,
    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    // HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService, { dataEncapsulation: false }),
    BrowserModule,
    BrowserModule,
    ThreadRoutesModule,
    BoardRoutesModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule {}
