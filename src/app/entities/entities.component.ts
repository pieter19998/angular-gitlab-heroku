import { Component, OnInit } from '@angular/core'
import { UseCase } from '../../models/usecase.model'

@Component({
  selector: 'app-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss']
})
export class EntitiesComponent implements OnInit {
  useCases: UseCase[] = [
    {
      _id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
      ],
      actor: 'Reguliere gebruiker',
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd'
    },
    {
      _id: 'UC-02',
      name: 'Naam',
      description: 'Hiermee kan een administrator ...',
      scenario: ['Stap 1', 'Stap 2', 'Stap 3'],
      actor: 'Administrator',
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt.'
    }
  ]
  constructor() {}

  ngOnInit() {}
}
