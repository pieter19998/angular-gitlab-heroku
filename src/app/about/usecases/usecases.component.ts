import { Component, OnInit } from '@angular/core'
import { UseCase } from '../../../models/usecase.model'
import { UseCasesService } from '../../services/use-cases.service'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  useCases: UseCase[];
  title = 'image board';

  constructor(private usecaseService: UseCasesService) {}

  ngOnInit() {
    this.getUsecases()
  }

  getUsecases(): void {
    this.usecaseService.getUsecases().subscribe(useCases => (this.useCases = useCases))
  }
}
