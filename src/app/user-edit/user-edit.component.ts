import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../services/user.service';
import {User} from '../../models/User.model';
import {AuthenticationService} from '../services/authentication.service';
import {Observable, Subscription} from 'rxjs';
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  editUserForm: FormGroup;
  user: User;
  constructor(private userService: UserService, private formBuilder: FormBuilder, private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.getUser();
    this.editUserForm = new FormGroup({
      email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
      userName: new FormControl(null, [Validators.required, this.validuserName.bind(this)])
    })
  }

  onSubmit() {
    if (this.editUserForm.valid) {
      const userName = this.editUserForm.value.userName;
      const email = this.editUserForm.value.email
      this.userService.editUser(email, userName)
    } else {
      console.error('loginForm invalid')
    }
  }

  getUser(): void {

    this.userService.getUser().subscribe(user => (this.user = user))
  }

  validEmail(control: FormControl): { [s: string]: boolean } {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const email = control.value;
    const regexp = new RegExp(re);
    if (regexp.test(email) !== true) {
      return {email: false}
    } else {
      return null
    }
  }
  //
  // validPassword(control: FormControl): { [s: string]: boolean } {
  //   const password = control.value;
  //   const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}');
  //   const test = regexp.test(password);
  //   if (regexp.test(password) !== true) {
  //     return {password: false}
  //   } else {
  //     return null
  //   }
  // }

  validuserName(control: FormControl): { [s: string]: boolean } {
    return null
  }

}
