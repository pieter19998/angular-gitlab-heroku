import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { BoardComponent } from './board.component'
import { ThreadCreateComponent } from '../threads/thread-create/thread-create.component'
import { ThreadListComponent } from '../threads/thread-list/thread-list.component'

const BoardRoutes: Routes = [
  { path: 'board/:id', component: BoardComponent
  },

  // { path: 'board/:id', component: BoardComponent, children: [
  //     { path: 'thread/:id', component: ThreadListComponent }

  // { path: 'board/:id/thread/create', component: ThreadCreateComponent
  // },
]

@NgModule({
  imports: [RouterModule.forChild(BoardRoutes)],
  exports: [RouterModule]
})
export class BoardRoutesModule {}
