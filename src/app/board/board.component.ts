import {Component, OnDestroy, OnInit} from '@angular/core';
import {Board} from '../../models/board.model'
import {BoardService} from '../services/board.service'
import {ActivatedRoute} from '@angular/router';
import {interval, Observable, Subscription} from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss'],

})
export class BoardComponent implements OnInit, OnDestroy {
  boards: Board[];
  isLoggedIn$: Observable<boolean>;
  userNameSubscription: Subscription;
  userName = '';
  public isCollapsed = true;

  constructor(private boardService: BoardService,
              private route: ActivatedRoute,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    // interval(3000).subscribe(x => this.getBoard())
    this.getBoard()
    this.isLoggedIn$ = this.authService.userIsLoggedIn;
    this.userNameSubscription = this.authService.userFullName.subscribe(name => this.userName = name);
  }

  getBoard(): void {
    this.route.paramMap.subscribe(params => {
      this.boardService.getBoard(params.get('id')).subscribe(boards => (this.boards = boards))
    })
  }

  ngOnDestroy() {
    this.userNameSubscription.unsubscribe();
  }
}
