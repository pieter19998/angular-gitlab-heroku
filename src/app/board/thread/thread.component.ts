import { Component, OnInit, Input } from '@angular/core'
import { Thread } from '../../../models/thread.model'
import {Router, ActivatedRoute} from '@angular/router';
// @Component({
//   selector: 'app-thread',
//   template: `
//       <div class="card" style="width: 14rem;" >
//           <div class="card-header text-center">
//               {{thread.title}}
//           </div>
//           <img src="{{thread.image}}" class="card-img-top" alt="...">
//           <div class="card-body">
//               <h6 class="card-subtitle mb-2 text-muted text-left">poster: {{thread.user.userName}}</h6>
//               <p class="card-text">       {{thread.content}}</p>
// <!--              <a href="#" class="btn btn-primary">Go somewhere</a>-->
//               <a class="col text-center btn btn-primary " [routerLink]="['/thread/' + thread._id]">Thread</a>
//           </div>
//       </div>
//   `
// })

@Component({
  selector: 'app-thread',
  styleUrls: ['thread.component.scss'],
  template: `
      <div class="card mb-3" style="max-width: 540px;">
          <div class="row no-gutters">
              <div class="col-md-4">
                  <img src="{{thread.image}}" class="card-img" alt="...">
              </div>
              <div class="col-md-8">
                  <div class="card-body">
                      <h5 class="card-title">{{thread.title}}</h5>
                      <p class="card-text">{{thread.content}}</p>
                      <p class="card-text"><small class="text-muted">{{thread.postDatetime | date :'full' }}</small></p>
                      <p class="card-text"><small class="text-muted">{{thread.user.userName}}</small></p>
                      <a class="col text-center btn btn-primary " [routerLink]="['/thread/' + thread._id]">Thread</a>
<!--                      <a class="btn btn-lg bt-primary" (click)= goToSibling() role="button">Click </a>-->

                  </div>
              </div>
          </div>
      </div>
  `
})

export class ThreadComponent implements OnInit {
  @Input() thread: Thread

  constructor(public router: Router, public route: ActivatedRoute) {}

  ngOnInit() {
    console.dir(this.thread)
  }

  // goToSibling(){
  //   this.router.navigate(['/thread/' + this.thread._id], { relativeTo: this.route });
  // }
}
