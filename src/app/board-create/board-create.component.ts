import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BoardService} from '../services/board.service';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../services/authentication.service';
import {Board} from '../../models/board.model';

@Component({
  selector: 'app-board-create',
  templateUrl: './board-create.component.html',
  styleUrls: ['./board-create.component.scss']
})
export class BoardCreateComponent implements OnInit {
  boardForm: FormGroup
  isLoggedIn$: Observable<boolean>;
  constructor(private boardService: BoardService, private formBuilder: FormBuilder, private authService: AuthenticationService) { }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn;
    this.boardForm = new FormGroup({
      name: new FormControl(null, [Validators.required, this.validName.bind(this)]),
      description: new FormControl(null, [Validators.required, this.validDescription.bind(this)])
    })
  }

  onSubmit() {
    if (this.boardForm.valid) {
      const name = this.boardForm.value.name;
      const description = this.boardForm.value.description;
      console.log(name)
      console.log(description)
      const board = new Board(null, name, description, null)
      this.boardService.addBoard(board)
    } else {
      console.error('loginForm invalid')
    }
  }

  validDescription(control: FormControl): { [s: string]: boolean } {
    return null
    // const content = control.value;
    // if (content.length >= 1) {
    //   return  null
    // } else {
    //   return {content: false}
    // }
  }

  validName(control: FormControl): { [s: string]: boolean } {
    return null
    // const content = control.value;
    // if (content.length >= 1) {
    //   return  null
    // } else {
    //   return {content: false}
    // }
  }
}
