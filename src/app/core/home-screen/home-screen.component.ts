import { Component, OnInit } from '@angular/core';
import {BoardService} from '../../services/board.service'
import {Board} from '../../../models/board.model';

@Component({
  selector: 'app-home-screen',
  templateUrl: './home-screen.component.html',
  styleUrls: ['./home-screen.component.scss']
})
export class HomeScreenComponent implements OnInit {
  boards: Board[];

  constructor(private boardService: BoardService) {
  }

  ngOnInit() {
    this.getBoards()
  }

  getBoards(): void {
    this.boardService.getBoards().subscribe(boards => (this.boards = boards))
  }
}
