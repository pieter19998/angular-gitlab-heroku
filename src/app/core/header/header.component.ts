import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() apptitle: string;
  isLoggedIn$: Observable<boolean>;
  isAdmin$: Observable<boolean>;
  userNameSubscription: Subscription;
  // token: Observable<string>;
  userName = '';

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.userIsLoggedIn;
    this.isAdmin$ = this.authService.userIsAdmin;
    this.userNameSubscription = this.authService.userFullName.subscribe(name => this.userName = name);
    // this.token = this.authService.userToken;
  }

  onLogout() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.userNameSubscription.unsubscribe();
  }
}

