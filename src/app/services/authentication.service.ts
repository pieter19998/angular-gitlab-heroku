import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {User} from '../../models/User.model';
import {Router} from '@angular/router';
import {AlertService} from '../alert/alert.service';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public readonly redirectUrl: string = '/';
  private readonly headers = new HttpHeaders({'Content-Type': 'application/json'});
  private readonly currentUser = 'currentuser';
  private readonly currentToken = 'token';
  // private readonly authUrl = 'http://localhost:3000/api/user/login'; // URL to web api
  private readonly authUrl = `${environment.apiUrl}/api/user/login`; // URL to web api
  private isLoggedInUser = new BehaviorSubject<boolean>(false);
  private isAdminUser = new BehaviorSubject<boolean>(false);
  private isPlainUser = new BehaviorSubject<boolean>(false);
  private loggedInUserName = new BehaviorSubject<string>('');
  private loggedInId = new BehaviorSubject<string>('');
  private loggedInToken = new BehaviorSubject<string>('');
  private userIsAllowed = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router, private alertService: AlertService) {
    this.getCurrentUser().subscribe({
      next: (user: User) => {
        console.log(`${user.email} logged in`);
        this.isLoggedInUser.next(true);
        this.loggedInUserName.next(user.userName);
        this.loggedInId.next(user._id)
      },
      error: message => {
        this.isLoggedInUser.next(false);
        this.isAdminUser.next(false);
        this.isPlainUser.next(false)
        this.router.navigate(['/login']);
      }
    });

    this.getToken().subscribe({
      next: (token: string) => {
        this.loggedInToken.next(token);
      }
    })
  }

  get userFullName(): Observable<string> {
    console.log('userFullName ' + this.loggedInUserName.value);
    return this.loggedInUserName.asObservable()
  }

  /**
   *
   */
  get userIsLoggedIn(): Observable<boolean> {
    console.log('userIsLoggedIn() ' + this.isLoggedInUser.value);
    return this.isLoggedInUser.asObservable()
  }

    userIsUser(id): Observable<boolean> {
    console.log('userIsLoggedIn() ' + this.isLoggedInUser.value);

    if (id === this.loggedInId) {
      this.userIsAllowed.next(true)
      return this.userIsAllowed.asObservable()
    }
    return this.isLoggedInUser.asObservable()
  }

  /**
   *
   */
  get userId(): Observable<string> {
    console.log('userId() ' + this.loggedInId.value);
    return this.loggedInId.asObservable()
  }

  /**
   *
   */
  get userIsAdmin(): Observable<boolean> {
    console.log('userIsAdmin() ' + this.isAdminUser.value);
    return this.isAdminUser.asObservable()
  }

  /**
   *
   */
  get userToken(): Observable<string> {
    console.log('userToken() ' + this.loggedInToken.value);
    return this.loggedInToken.asObservable()
  }

  /**
   *
   */
  get userIsPlain(): Observable<boolean> {
    console.log('userIsPlain() ' + this.isPlainUser.value);
    return this.isPlainUser.asObservable()
  }

  login(email: string, password: string) {
    console.log('login');
    console.log(this.authUrl);

    return this.http
      .post(
        this.authUrl,
        {email: email, password: password},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          const currentUser = new User(response._id, response.userName, response.email, response.password, response.role);
          console.dir(currentUser);
          this.saveCurrentUser(currentUser, response.token);

          // Notify all listeners that we're logged in.
          this.isLoggedInUser.next(true);
          this.loggedInUserName.next(currentUser.userName);
          this.loggedInId.next(currentUser._id);

          console.log('success ingelogt');
          this.alertService.success('U bent ingelogd')
          //
          // If redirectUrl exists, go there
          this.router.navigate([this.redirectUrl]);
          return true;
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }

  logout() {
    console.log('logout');
    localStorage.removeItem(this.currentUser);
    localStorage.removeItem(this.currentToken);
    this.isLoggedInUser.next(false);
    this.isAdminUser.next(false);
    this.alertService.success('U bent uitgelogd')
    console.log('U bent uitgelogd')
  }

  private getCurrentUser(): Observable<User> {
    return Observable.create(observer => {
      const localUser: any = JSON.parse(localStorage.getItem(this.currentUser));
      if (localUser) {
        console.log('localUser found');
        observer.next(new User(localUser._id, localUser.userName, localUser.email, localUser.password, localUser.role));
        observer.complete()
      } else {
        console.log('NO localUser found');
        observer.error('NO localUser found');
        observer.complete()
      }
    })
  }

  private getToken(): Observable<string> {
    return Observable.create(observer => {
      const token: any = localStorage.getItem(this.currentToken);
      // console.log(localStorage.getItem(this.currentToken));
      if (token) {
        console.log('token found');
        observer.next(token);
        observer.complete()
      } else {
        console.log('NO token found');
        observer.error('NO tokem found');
        observer.complete()
      }
    })
  }

  private saveCurrentUser(user: User, token: string): void {
    localStorage.setItem(this.currentUser, JSON.stringify(user));
    localStorage.setItem(this.currentToken, token)
  }
}
