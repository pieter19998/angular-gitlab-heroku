import { TestBed } from '@angular/core/testing';

import { ThreadService } from './thread.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UseCasesService} from './use-cases.service';
import {RouterModule} from '@angular/router';
import {ThreadRoutesModule} from '../threads/thread-routing';

describe('ThreadService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, ThreadRoutesModule , RouterModule],
    providers: [ThreadService]
  }));

  it('should be created', () => {
    const service: ThreadService = TestBed.get(ThreadService);
    expect(service).toBeTruthy();
  });
});
