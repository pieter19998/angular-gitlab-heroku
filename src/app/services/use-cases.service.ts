import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'

import { Observable, of } from 'rxjs'
import { catchError, map, tap } from 'rxjs/operators'

import { UseCase } from '../../models/usecase.model'
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UseCasesService {
  // private useCasesUrl = 'http://localhost:3000/api/usecase' // URL to web api
  private useCasesUrl = `${environment.apiUrl}/api/usecase`; // URL to web api
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  constructor(private http: HttpClient) {}

  /** GET UseCases from the server */
  getUsecases(): Observable<UseCase[]> {
    return this.http.get<UseCase[]>(this.useCasesUrl).pipe(
      tap(_ => this.log('fetched heroes')),
      catchError(this.handleError<UseCase[]>('getUsecases', []))
    )
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error) // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }

  private log(message: string) {
    console.log(message)
  }
}
