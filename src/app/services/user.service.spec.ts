import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { UserService } from './user.service'
import {RouterModule} from '@angular/router';
import {ThreadRoutesModule} from '../threads/thread-routing';

describe('UserService', () => {
  beforeEach(() => TestBed.configureTestingModule({

    imports: [HttpClientTestingModule, ThreadRoutesModule],
    providers: [UserService]
  }));

  it('should be created', () => {
    const service: UserService = TestBed.get(UserService);
    expect(service).toBeTruthy();
  });
})
