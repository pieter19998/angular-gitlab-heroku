import { InMemoryDbService } from 'angular-in-memory-web-api'
import { UseCase } from '../../models/usecase.model'
import { Injectable } from '@angular/core'
import {Thread} from '../../models/thread.model';
import {Reply} from '../../models/reply';

@Injectable({
  providedIn: 'root'
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const useCases = [
      {
        id: 'UC-01',
        name: 'Inloggen',
        description: 'Hiermee logt een bestaande gebruiker in.',
        scenario: [
          'Gebruiker vult email en password in en klikt op Login knop.',
          'De applicatie valideert de ingevoerde gegevens.',
          'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Geen',
        postcondition: 'De actor is ingelogd'
      },
      {
        id: 'UC-02',
        name: 'Account kan worden aangemaakt',
        description: 'Een gebruiker kan accounts aanmaken',
        scenario: [
          'Gebruiker gaat naar register pagina',
          'Gebruiker voert gegevens in',
          'gebruiker heeft een account aangemaakt'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Geen',
        postcondition: 'De user is geregistreert'
      },
      {
        id: 'UC-03',
        name: 'Een gebruiker kan een Board aanmaken',
        description: 'Een gebruiker kan Board aanmaken',
        scenario: [
          'Gebruiker gaat naar Create pagina',
          'Gebruiker voert gegevens in',
          'gebruiker heeft een board aangemaakt'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Gebruiker is ingelogt',
        postcondition: 'De user heeft een board aangemaakt'
      },
      {
        id: 'UC-04',
        name: 'Een gebruiker kan een board aanmaken',
        description: 'Een gebruiker kan board aanmaken',
        scenario: [
          'Gebruiker gaat naar een Board pagina',
          'Gebruiker klikt op Create board button',
          'Gebruiker vult gegevens in en submit',
          'Thread is aangemaakt'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Gebruiker is ingelogt',
        postcondition: 'De Gebruiker heeft een board aangemaakt'
      },
      {
        id: 'UC-05',
        name: 'Een gebruiker kan een Thread aanmaken',
        description: 'Een gebruiker kan Thread aanmaken',
        scenario: [
          'Gebruiker gaat naar een Board pagina',
          'Gebruiker klikt op Create Thread button',
          'Gebruiker vult gegevens in en submit',
          'Thread is aangemaakt'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Gebruiker is ingelogt',
        postcondition: 'De user heeft een thread aangemaakt'
      },
      {
        id: 'UC-06',
        name: 'Een gebruiker kan een post aanmaken',
        description: 'Een gebruiker kan post aanmaken',
        scenario: [
          'Gebruiker gaat naar een Board pagina',
          'Gebruiker klikt op een Thread ',
          'Gebruiker vult gegevens in en submit',
          'Thread is aangemaakt'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Gebruiker heeft een post geplaatst',
        postcondition: 'De user heeft een post aangemaakt'
      },
      {
        id: 'UC-07',
        name: 'Een admin crud functionaliteit gebruiken op gerbuikers',
        description: 'Een admin crud functionaliteit gebruiken op gerbuikers',
        scenario: [
          'Administrator gaat naar een de dashboard',
          'Administrator klikt op een gebruiker lijst',
          'Admin kan krijgt lijst met gebruikers met crud opties'
        ],
        actor: 'Reguliere gebruiker',
        precondition: 'Administrator is ingelogt',
        postcondition: 'crud functionaliteit'
      },
      {
        id: 'UC-08',
        name: 'Een admin kan posts verwijderen van gebruikers',
        description: 'Een admin kan posts verwijderen van gebruikers',
        scenario: [
          'Administrator gaat naar een thread',
          'gebruiker klikt op delete bij een post',
          'post is verwijdert uit de thread'
        ],
        actor: 'administrator',
        precondition: 'Administrator is ingelogt',
        postcondition: 'post is verwijdert'
      }
    ]

    const User = [
      {
        id: undefined,
        name: 'jan',
        email: 'test',
        password: 'password'
      }
    ]

    const Board = [
      {
        id: 'v',
        name: '/v/',
        threads: [
          {id: 1 , title: 'test thread1' , user: 'jan' , content: '1', post: null, image: '../../'},
          {id: 2 , title: 'test thread2' , user: 'willem' , content: '2',  post: null},
          {id: 3 , title: 'test thread3' , user: 'klaas' , content: '3',  post: null},
          {id: 4 , title: 'test thread4' , user: 'paul' , content: '4',  post: null},
          {id: 5 , title: 'test thread5' , user: 'vincent' , content: '5', post: null},
        ]
      }
    ]

    return { useCases, User, Board}
  }
}
