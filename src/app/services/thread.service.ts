import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Thread} from '../../models/thread.model';
import {catchError, map, tap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class ThreadService {
  // private threadUrl = 'http://localhost:3000/api/thread'; // URL to web api
  private threadUrl = `${environment.apiUrl}/api/thread`; // URL to web api
  public readonly redirectUrl: string = '/board/';
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient, private router: Router) {}

  // /** GET Board from the server */
  // getThread(id): Observable<Thread[]> {
  //   return this.http.get<Thread[]>(this.threadUrl + '/' + id).pipe(
  //     tap(_ => console.log('fetched thread')),
  //     catchError(this.handleError<Thread[]>('getThread', []))
  //   )
  // }

  /** GET Board from the server */
  getThread(id): Observable<Thread[]> {
    return this.http.get<Thread[]>(this.threadUrl + '/' + id).pipe(
      tap(_ => console.log('fetched thread')),
      catchError(this.handleError<Thread[]>('getThread', []))
    )
  }

  /** POST: add a new thread to the server */
  addThead(thread: Thread , board) {
    console.log('addUser');
    console.log(this.threadUrl);
    console.dir(thread);

    return this.http
      .post(
        this.threadUrl + '/' + board._id,
        { image: thread.image, title: thread.title, content: thread.content, postDatetime: thread.postDatetime, user: thread.user},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          // this.alertService.success('U bent ingelogd')
          console.log('success created')
          this.router.navigate([this.redirectUrl + board.name]);
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          // this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
