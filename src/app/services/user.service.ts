import {Injectable} from '@angular/core'
import {HttpClient, HttpHeaders} from '@angular/common/http'
import {User} from '../../models/User.model'
import {Observable, of, Subscription} from 'rxjs'
import {catchError, map, tap} from 'rxjs/operators'
import {Router} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  public readonly redirectUrl: string = '/login';
  tokenSubscription: Subscription;
  token: string;
  userIdSubscription: Subscription;
  userId: string;
  // private userUrl = 'http://localhost:3000/api/user'; // URL to web api
  private userUrl = `${environment.apiUrl}/api/user`; // URL to web api
  private readonly headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient, private router: Router, private authenticationService: AuthenticationService) {
    this.tokenSubscription = this.authenticationService.userToken.subscribe(id => this.token = id);
    this.userIdSubscription = this.authenticationService.userId.subscribe(id => this.userId = id)
  }


  /** GET user from the server */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userUrl).pipe(catchError(this.handleError<User[]>('getUsers', [])))
  }

  getUser(): Observable<User> {

    console.log(this.userId + 'ZZZZ');
    return this.http.get<User>(this.userUrl + '/' + this.userId).pipe(catchError(this.handleError<User>('getUser',)))
  }

  /** POST: add a new user to the server */
  addUser(email: string, password: string, userName: string) {
    console.log('addUser');
    console.log(this.userUrl);
    return this.http
      .post(
        this.userUrl,
        {email: email, password: password, userName: userName},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {

          // this.alertService.success('U bent ingelogd')
          console.log('success geregistreet');

          this.router.navigate([this.redirectUrl]);
          return true;
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          // this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }

  editUser(email: string, userName: string) {
    console.log('editUser');
    console.log(email + userName);
    // this.headers.set('token', this.token);
    return this.http
      .put(
        this.userUrl + '/' + this.token,
        {email: email, userName: userName},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          // const currentUser = new User(response._id, response.userName,response.email,response.password,response.role);
          // console.dir(currentUser);
          // this.saveCurrentUser(currentUser, response.token);

          // Notify all listeners that we're logged in.
          // this.isLoggedInUser.next(true);
          // this.loggedInUserName.next(currentUser.userName);
          // currentUser.hasRole(UserRole.Admin).subscribe(result => this.isAdminUser.next(result));
          // currentUser.hasRole(UserRole.Basic).subscribe(result => this.isPlainUser.next(result));

          // this.alertService.success('U bent ingelogd')
          console.log('success geupdate');
          //
          // If redirectUrl exists, go there
          this.router.navigate([this.redirectUrl]);
          return true;
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          // this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }
}
