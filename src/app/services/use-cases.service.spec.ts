import { TestBed } from '@angular/core/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {HttpClientModule} from '@angular/common/http';
import { UseCasesService } from './use-cases.service'

describe('UseCasesService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [UseCasesService]
  }));

  it('should be created', () => {
    const service: UseCasesService = TestBed.get(UseCasesService);
    expect(service).toBeTruthy();
  });
})
