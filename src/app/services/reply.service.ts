import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';
import {Reply} from '../../models/reply';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReplyService {
  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  // private readonly threadReplyUrl = 'http://localhost:3000/api/reply/'; // URL to web api
  private readonly threadReplyUrl = `${environment.apiUrl}/api/reply`;; // URL to web api

  constructor(private http: HttpClient,  private router: Router) {
  }

  /** POST: add a new thread to the server */
  addReply(reply: Reply, id , type) {
    console.log('addReply');
    console.log(this.threadReplyUrl + type + '/' + id);
    console.dir(reply);

    return this.http
      .post(
        this.threadReplyUrl + type + '/' + id,
        {content: reply.content, image: reply.image , postDatetime: reply.postDatetime, user: reply.user},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          // this.alertService.success('U bent ingelogd')
          console.log('success created')
          return true;
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          // this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }
}
