import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Board} from '../../models/board.model';
import {catchError, map, tap} from 'rxjs/operators';
import {Thread} from '../../models/thread.model';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'} )
  };
  // private useCasesUrl = 'api/Board'; // URL to web api
  // private boardUrl = 'http://localhost:3000/api/board'; // URL to web api
  private boardUrl = `${environment.apiUrl}/api/board`; // URL to web api

  private readonly headers = new HttpHeaders({ 'Content-Type': 'application/json' })
  private redirectUrl = '/home';

  constructor(private http: HttpClient, private router: Router) {}

  /** GET Board from the server */
  getBoard(id): Observable<Board[]> {
    return this.http.get<Board[]>(this.boardUrl + '/' + id).pipe(
      tap(_ => this.log('fetched board')),
      catchError(this.handleError<Board[]>('getBoard', []))
    )
  }

  /** GET Boards from the server */
  getBoards(): Observable<Board[]> {
    return this.http.get<Board[]>(this.boardUrl).pipe(
      tap(_ => this.log('fetched boards')),
      catchError(this.handleError<Board[]>('getBoards', []))
    )
  }

  addBoard(board: Board) {
    console.log('addUser');
    console.log(this.boardUrl);
    console.dir(board);

    return this.http
      .post(
        this.boardUrl,
        {name: board.name, description: board.description},
        {headers: this.headers}
      )
      .pipe(
        map(response => response),
        tap(console.log)
      )
      .subscribe({
        next: response => {
          // this.alertService.success('U bent ingelogd')
          console.log('success created')
          this.router.navigate([this.redirectUrl]);
          return true;
        },
        error: err => {
          // console.error('Error logging in: ' + err);
          // this.alertService.error('Invalid credentials')
          console.log(err)
        }
      })
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T)
    }
  }

  private log(message: string) {
    console.log(message)
  }
}
