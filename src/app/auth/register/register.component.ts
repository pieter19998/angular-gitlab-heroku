import {Component, OnDestroy, OnInit} from '@angular/core'
import {User} from '../../../models/User.model'
import {UserService} from '../../services/user.service'
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms'
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup
  subs: Subscription

  constructor(private userService: UserService, private formBuilder: FormBuilder) {
  }


  onSubmit() {
    if (this.registerForm.valid) {
      const email = this.registerForm.value.email;
      const password = this.registerForm.value.password;
      const userName = this.registerForm.value.userName;
      this.userService.addUser(email, password, userName)
    } else {
      console.error('loginForm invalid')
    }
  }

  ngOnInit() {
    this.registerForm = new FormGroup({
      email: new FormControl(null, [Validators.required, this.validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, this.validPassword.bind(this)]),
      // passwordRepeat: new FormControl(null, [Validators.required, this.validPasswordRepeat.bind(this)]),
      userName: new FormControl(null, [Validators.required, this.validuserName.bind(this)])
    })
  }

  validEmail(control: FormControl): { [s: string]: boolean } {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const email = control.value;
    const regexp = new RegExp(re);
    if (regexp.test(email) !== true) {
      return {email: false}
    } else {
      return null
    }
  }

  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value;
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}');
    const test = regexp.test(password);
    if (regexp.test(password) !== true) {
      return {password: false}
    } else {
      return null
    }
  }

  // validPasswordRepeat(control: FormControl): { [s: string]: boolean } {
  //   const password = control.value.password;
  //   const repeatPassword = control.value.passwordRepeat;
  //   if (password !== repeatPassword) {
  //     return {password: false}
  //   } else {
  //     return null
  //   }
  // }
  validuserName(control: FormControl): { [s: string]: boolean } {
    return null
    }
}
