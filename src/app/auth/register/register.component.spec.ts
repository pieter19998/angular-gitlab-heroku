import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RegisterComponent } from './register.component'
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { UserService } from '../../services/user.service'
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms'

describe('RegisterComponent', () => {
  let component: RegisterComponent
  let fixture: ComponentFixture<RegisterComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [HttpClientTestingModule , FormsModule, ReactiveFormsModule],
      providers: [UserService]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
