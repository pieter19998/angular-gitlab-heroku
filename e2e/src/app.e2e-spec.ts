import { AppPage } from './app.po'
import { browser, logging } from 'protractor'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User } from '../../src/models/User.model'

describe('workspace-project App', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
  })

  it('should display "Use cases app" as navbar-brand', () => {
    page.navigateTo()
    expect(page.getTitleText()).toEqual('angular-gitlab-heroku')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser
      .manage()
      .logs()
      .get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    )
  })
})
